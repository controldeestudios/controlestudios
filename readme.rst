###################
Control de Estudios
###################

Sistema de Control de Estudios para la Facultad Experimental de Ciencias y Tecnología de la Universidad de Carabobo.
Repositorio creado el 8 de Marzo de 2017 por Luis Pérez miembro de la DTIC@FACyT

*********
Componentes
*********

-  CodeIgniter 3.1.3
-  Modulo HMVC
-  Integración con Bootstrap
-  Plantilla Light Bootstrap Dashboard Master (FREE)