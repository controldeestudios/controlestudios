<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3"></div>
		<div class="col-lg-6">
			<h1 class="text-center text-danger"><i class="fa fa-exclamation-circle fa-3x"></i></h1>
			<h1 class="text-center text-danger">Error 404</h1>
			<p class="text-center h3 text-info">Recurso no encontrado. <br>Disculpe, el archivo solicitado no ha sido localizo en el servidor.</p>
		</div>
		<div class="col-lg-3"></div>
	</div>
</div>