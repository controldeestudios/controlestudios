<!-- <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Your description">
	<meta name="keywords" content="Your,Keywords">
	<meta name="author" content="ResponsiveWebInc">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div class="container"> -->
	
	</div>

    <footer class="footer">
      <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 text-center">
                <a href="http://www.facyt.uc.edu.ve/"><img src="<?php echo base_url() ?>assets/img/footer-facyt.png" class="padding-top-10 img-responsive img-center"></a>
            </div>
            <!-- <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 text-center">
                <span class="text-muted">Dirección de Telecomunicación Información y Computación | FACyT | UC</span>
            </div> -->
            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 text-center">
                <a href="http://www.facyt.uc.edu.ve/dir-tic" style="text-decoration: none;">
                    <img src="<?php echo base_url(); ?>assets/img/footer-dtic.png" class="padding-top-5 img-responsive img-center">
                    <span class="text-white text-small">Desarrollado por: Dirección TIC | FACyT</span>
                </a>
            </div>
        </div>
      </div>
    </footer>

	<!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="<?php echo base_url() ?>vendor/raphael/raphael.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>vendor/morrisjs/morris.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

</body>
</html>