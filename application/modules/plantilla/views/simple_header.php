<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo (isset($title) && !empty($title)) ? $title.' | Control de Estudios FACyT' : 'Control de Estudios FACyT' ?></title>

	<meta name="description" content="Sistema de Control de Estudios">
	<meta name="keywords" content="">
	<meta name="author" content="Dirección de Tecnología e Informacion de FACyT">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/ico-facyt.png" type="image/png" />

	<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/sbadmin2/personal.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/sbadmin2/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/sbadmin2/sticky-footer.css" rel="stylesheet">
</head>

<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		    <div class="navbar-header">
		        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		        </button>
		        <a class="navbar-brand" href="<?php echo base_url() ?>principal"><img class="pull-right img-responsive" src="<?php echo base_url(); ?>assets/img/navbar-header-logo.png"></a>

		    </div>
		</nav>
		
<!-- 	</div>
</body>
</html> -->