<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo (isset($title) && !empty($title)) ? $title.' | Control de Estudios FACyT' : 'Control de Estudios FACyT' ?></title>

	<meta name="description" content="Sistema de Control de Estudios de la Facultad Experimental de Ciencias y Tecnología de la Universidad de Carabobo">
	<meta name="keywords" content="">
	<meta name="author" content="Dirección de Tecnología e Informacion de FACyT">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/ico-facyt.png" type="image/png" />

	<!-- Hojas de Estilo -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/bootstrap-theme.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/sbadmin2/metisMenu.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/css/sbadmin2/sb-admin-2.css" rel="stylesheet">

    <!-- Custom CSS -->
	<link href="<?php echo base_url() ?>assets/css/sbadmin2/personal.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/sbadmin2/font-awesome.css" rel="stylesheet" type="text/css">

    <!-- Sticky Footer CSS -->
    <link href="<?php echo base_url(); ?>assets/css/sbadmin2/sticky-footer.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div id="wrapper">
	<!-- LOS ELEMENTOS DEL MENU ESTÁN CONTENIDOS EN ESTE ARCHIVO -->
		<?php include_once('menu_principal.php'); ?>
<!-- 	</div>
</body>
</html> -->