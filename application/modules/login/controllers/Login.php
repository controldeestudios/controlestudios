<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('header_login');
		$this->load->view('login_page');
		$this->load->view('footer_login');
	}
}
