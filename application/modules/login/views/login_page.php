<style type="text/css">
	.form-signin {
	    max-width: 480px;
	}
	.head-color{
		background-color: #b9b9b9;
		padding: 15px;
		border-top-left-radius: 7px;
		border-top-right-radius: 7px;
	}
	.body-color{
		background-color: #ffffff;
		padding: 15px;
		border-bottom-left-radius: 7px;
		border-bottom-right-radius: 7px;
	}
	.no-margin { margin: 0px; }
	.form-control-mail{
		border-top-right-radius: 5px;
		border-top-left-radius: 5px;
		border-bottom-left-radius: 0px;
		border-bottom-right-radius: 0px;
		border-bottom: hidden;
	}
	.form-control-password{
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;
	}
</style>

<form class="form-signin">
	<div class="row head-color">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-1"></div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-10"><img src="<?php echo base_url(); ?>assets/img/logo-facyt.png" class="img-responsive" title="Logo FACyT"></div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-1"></div>
	<!-- </div>
	<div class="row"> -->
		<!-- <div class="col-lg-12">
			<p class="text-center text-bold">Bienvenido al</p>
			<h3 class="text-center form-signin-header">Sistema de Información y Control Estudiantil</h3>
		</div> -->
	</div>
	<!-- <br> -->
	<div class="row body-color">
		<p class="form-signin-heading text-center h4 no-margin">Inicio de Sesión</p>
		<label for="inputEmail" class="sr-only">Email address</label>
		<input id="inputEmail" class="form-control form-control-mail" placeholder="Usuario" required autofocus>
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" id="inputPassword" class="form-control form-control-password" placeholder="Clave" required>
		<!-- <hr> -->
		<!--<div class="checkbox text-center">
			<label class="text-center">
				<input type="checkbox" class="text-center" value="remember-me"> Recordar
			</label>
		</div>-->
		<button class="btn btn-primary btn-block" type="submit">Iniciar Sesión</button>
	</div>
</form>
<hr>
<a href="<?php echo base_url(); ?>principal" class="btn btn-primary">/principal</a>