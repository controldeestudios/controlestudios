<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('plantilla/header');
		$this->load->view('principal');
		$this->load->view('plantilla/footer');
	}

	public function error404()
	{
		$this->load->view('plantilla/simple_header');
		$this->load->view('error404');
		$this->load->view('plantilla/simple_footer');
	}
}
